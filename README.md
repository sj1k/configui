# configui

Yo, this is just a small UI to display and load configs.

It doesn't have any required backend so you should be able to hack it to work with whatever you got!

![hnnnng](https://my.mixtape.moe/cuvhkh.webm)

## Installing

#### Arch Linux

If you use Arch Linux, or a derivative, an AUR package is provided at [configui-git](https://aur.archlinux.org/packages/configui-git/).

#### Other OS

First you gotta install python3, pip3 and GObject, install these packages via your package manager.

`python3-gobject python3-gobject-devel python3-pip python3`


Then go ahead and clone this repo and install the requirements.

```
git clone https://gitlab.com/sj1k/configui
cd configui/

pip3 install -r requirements.txt --user
```

Aaaand that should do it, if you have issues please lemme know, ill place this in pypi if many people are interested so its easier to install.


## Input

It accepts JSON data on stdin (usually piped into the program) so you can create your own script to retrieve the data required then pipe the info into my UI.

Something like `./get_themes.sh | python3 configui.py --callback 'config_changer load {name}'`

Where {name} will be replaced with the name of the theme that is selected.

### JSON format

The json format is an array of themes

```json
[
   {
      "name": "My Theme",
      "wallpaper": "/path/to/image.png",
      "palette": [
         "0x000000",
         "0xFF0000",
         ...
      ]
   }
]
```

Pretty simple and easy to follow, You specify the name, wallpaper and pallette.
Pallette can be any length, I hope.

### CLI Arguments

(Because my --help output isn't that helpful)

**ALL of these can be placed in the ~/.config/configui.yaml**
They have the same name in both the CLI args and settings file.


Name                 | Description
-------------------- | -----------------------------
callback             | The command to run once you click a theme.
image_height         | The height to use for the images.
image_width          | The width to use for the images.
height               | The height of the program window. Accepts ints or % for percentages.
width                | The width of the program window. Accepts ints or % for percentages.
xpos                 | The x position of the program window. Accepts ints or % for percentages.
ypos                 | The y position of the program window. Accepts ints or % for percentages.
background_colour    | The background colour of the window. Invisible if not set.
single               | True if you want it to close after one choice, False otherwise.
timeout              | The timeout delay between moving your mouse off the program and the program closing itself. In seconds.
padding              | 4 numbers to represend padding on top, bottom, left and right.
align_bottom         | True is the images should align with the bottom of the window, False if they should align with the top.
dock                 | True if the window should act like a dock, False otherwise.


## SHOW ME WHAT YOU GOT

![Wew lad](images/2018-05-21-12-1526897541.png)

More screenies coming soon....

## Thanks

*You're welcome.*

+ u/enjuus for some naisu ideas
