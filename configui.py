import sys
import json
import subprocess
import argparse
import os
import gi
import threading
import time

import yaml

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, Gdk, GdkPixbuf, GObject


GObject.threads_init()


CONFIG = os.path.expanduser('~/.config/configui.yaml')


class MainWindow(Gtk.Window):

    _mouse_left_at = None

    def __init__(self, themes, settings):
        super().__init__()
        self.themes = themes
        self.settings = settings

    def main(self):
        screen = self.get_screen()

        widget_size = self.create_widgets()
        self.connect('delete-event', Gtk.main_quit)
        self.connect('leave-notify-event', self._mouse_leave)
        self.connect('enter-notify-event', self._mouse_enter)

        if self.settings['dock']:
            typehint = Gdk.WindowTypeHint.DOCK
        else:
            typehint = Gdk.WindowTypeHint.SPLASHSCREEN
        self.set_property('type-hint', typehint)
        self.set_property('skip-taskbar-hint', True)
        self.set_decorated(False)

        if self.settings['background_colour'] is None:
            self.set_app_paintable(True)
            visual = screen.get_rgba_visual()
            self.set_visual(visual)
        else:
            colour = Gdk.color_parse(self.settings['background_colour'])
            self.modify_bg(Gtk.StateType.NORMAL, colour)

        self.set_title('bar-dropdown')
        self.stick()
        self.set_resizable(False)
        self.set_border_width(0)

        swidth, sheight = (screen.get_width(), screen.get_height())
        width = parse_size(self.settings['width'], swidth) or swidth
        height = parse_size(self.settings['height'], sheight)

        difference = height - widget_size
        height = height + difference
        self.set_y_offset(height, difference)

        x = parse_size(self.settings['xpos'], swidth)
        if x is None:
            x = (swidth / 2 - width / 2)
        y = parse_size(self.settings['ypos'], sheight)
        if y is None:
            y = (sheight - height)

        self.move(x, y)

        self.set_size_request(width, height + difference)
        self.show_all()
        return None

    def set_y_offset(self, height, offset):
        for widget in self.widgets:
            if widget.pixbuf.get_height() == height:
                continue
            widget.offset_y = offset
        return None

    def _mouse_leave(self, *_):
        self._mouse_left_at = time.time()
        thread = threading.Thread(target=self._timed_shutdown)
        thread.daemon = True
        thread.start()
        return None

    def _mouse_enter(self, *_):
        self._mouse_left_at = None
        return None

    def _timed_shutdown(self):
        while self._mouse_left_at is not None:
            duration = time.time() - self._mouse_left_at
            if duration > self.settings['timeout']:
                Gtk.main_quit()
            time.sleep(1)
        return None

    def create_widgets(self):
        self.themebox = Gtk.HBox()
        self.widgets = []
        largest = 0

        for theme in self.themes:
            widget = ThemeWidget(theme, self.settings)
            self.themebox.pack_end(widget, True, True, 10)
            size = widget.pixbuf.get_height()
            if size > largest:
                largest = size
            self.widgets.append(widget)

        scrolled = Gtk.ScrolledWindow()
        scrolled.add(self.themebox)
        scrolled.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.NEVER)
        scrolled.connect('leave-notify-event', self._mouse_leave)
        scrolled.connect('enter-notify-event', self._mouse_enter)

        padd = map(int, self.settings['padding'][:4])
        padding = Gtk.Alignment()
        padding.add(scrolled)
        padding.set_padding(*padd)

        self.add(padding)
        return largest


class ThemeWidget(Gtk.DrawingArea):

    offset_y = 0

    def __init__(self, theme, settings):
        super().__init__()
        self.theme = theme
        self.settings = settings
        self.set_events(Gdk.EventMask.BUTTON_PRESS_MASK)
        self.connect('draw', self.expose)
        self.connect('button-press-event', self._clicked)

        widget_width = self.get_allocated_width()
        widget_height = self.get_allocated_height()
        height = parse_size(self.settings['image_height'], widget_height)
        width = parse_size(self.settings['image_height'], widget_width)

        image_path = os.path.expanduser(self.theme.get('wallpaper', None))
        if not os.path.exists(image_path):
            raise OSError('The image "{}" cannot be found.'.format(image_path))

        # JFC this function has a long name
        self.pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_size(
            image_path, width, height)

    def expose(self, widget, context):
        height = self.pixbuf.get_height()
        width = self.pixbuf.get_width()
        self.set_size_request(width, height)

        total_height = widget.get_allocated_height()
        palette = self.theme.get('palette', [])
        size = width / len(palette)

        if self.settings['align_bottom']:
            offset = total_height - size - height
        else:
            offset = 0.0
            total_height = height + size

        Gdk.cairo_set_source_pixbuf(context, self.pixbuf, 0.0, offset)
        context.paint()

        for index, value in enumerate(palette):
            position = size * (index)
            colour = Gdk.color_parse(value)
            context.set_source_rgb(colour.red/65535, colour.green/65535,
                                   colour.blue/65535)

            context.rectangle(position, total_height - size, size, size/2)
            context.fill()

        return None

    def _clicked(self, widget, event):
        if event.button == 1:
            command = self.settings['callback'].format(name=self.theme['name'])
            subprocess.call(command.split(' '))

            if self.settings['single']:
                Gtk.main_quit()
        return None


def parse_size(size, parent_size):
    if isinstance(size, int) or size is None:
        return size
    if size.endswith('%'):
        return parent_size / 100 * int(size.strip('%'))
    return int(size)


def load_settings(parser):
    parser.add_argument('--callback', '-c', default=None)
    parser.add_argument('--image_height', '-H', default=None)
    parser.add_argument('--image_width', '-W', default=None)
    parser.add_argument('--height', default=None)
    parser.add_argument('--width', '-w', default=None)
    parser.add_argument('--xpos', '-x', default=None)
    parser.add_argument('--ypos', '-y', default=None)
    parser.add_argument('--background_colour', '-b', default=None)
    parser.add_argument('--single', '-s', default=None, action='store_false')
    parser.add_argument('--timeout', '-t', default=None)
    parser.add_argument('--config', '-C', default=None)
    parser.add_argument('--padding', '-p', default=None, nargs='+')
    parser.add_argument('--align_bottom', '-a', default=None,
                        action='store_true')
    parser.add_argument('--dock', default=None, action='store_true')
    args = parser.parse_args()

    config = args.config or CONFIG
    data = {}
    if os.path.exists(config):
        with open(config, 'r') as f:
            data = yaml.load(f.read())

    if data is None:
        data = {}

    settings = {
        'callback': args.callback or data.get('callback', None),
        'image_height': args.image_height or data.get('image_width', 300),
        'image_width': args.image_width or data.get('image_height', 200),
        'height': args.height or data.get('height', 200),
        'width': args.width or data.get('width', None),
        'xpos': args.xpos or data.get('xpos', None),
        'ypos': args.ypos or data.get('ypos', None),
        'background_colour': (args.background_colour
                              or data.get('background_colour', None)
                              or data.get('background_color', None)),
        'single': args.single or data.get('single', False),
        'timeout': args.timeout or data.get('timeout', 1),
        'padding': args.padding or data.get('padding', [0, 0, 0, 0]),
        'align_bottom': args.align_bottom or data.get('align_bottom', False),
        'dock': args.dock or data.get('dock', False),
    }
    if settings['callback'] is None:
        raise ValueError('Callback is required.')
    return settings


def main():
    parser = argparse.ArgumentParser()

    try:
        settings = load_settings(parser)
    except ValueError:
        print('The callback param is required in the config file or argument.')
        parser.print_help()
        return None

    info = sys.stdin.readline()
    try:
        themes = json.loads(info)
    except json.decoder.JSONDecodeError:
        print('That was not json D:')
        return None

    window = MainWindow(themes, settings)
    window.main()
    Gtk.main()
    return None


if __name__ == "__main__":
    main()
